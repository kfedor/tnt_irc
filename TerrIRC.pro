#-------------------------------------------------
#
# Project created by QtCreator 2015-12-24T00:24:33
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += C++11

TARGET = TerrIRC
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    tabcontainer.cpp \
    serverconnection.cpp

HEADERS  += mainwindow.h \
    tabcontainer.h \
    serverconnection.h

FORMS    += mainwindow.ui
