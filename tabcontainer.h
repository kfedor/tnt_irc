#ifndef TABCONTAINER_H
#define TABCONTAINER_H

#include <QLineEdit>
#include <QLayout>
#include <QDebug>
#include <QMessageBox>
#include <QTextEdit>
#include <QListWidget>
#include <QLabel>
#include <QMenu>
#include <QTime>

class TabContainer : public QObject
{
    Q_OBJECT

public:        
    enum tab_type
    {
        tab_channel = 0x01,
        tab_private = 0x02,
        tab_server = 0x03
    };

    enum menu_action
    {
        op = 0x01,
        voice = 0x02,
        deop = 0x03,
        devoice = 0x04,
        kick = 0x05,
        ban = 0x06
    };

    struct user_list
    {
        QString nick;
        QString modes;
    };

    TabContainer() {};
    ~TabContainer() {};

    QWidget *addTab( tab_type );
    void setName( QString n ) { name = n; }
    QString getName() { return name; }
    QString addTime( QString data );
    void setTopic( QString text ) { topic->setText( text ); }
    void setMode( QString mode ) { channel_modes->setText( mode ); }
    void append( QString text ) { edit->append( addTime(text) ); }    

    void clearList();
    void redrawList();
    bool changeList( QString oldnick, QString newnick );
    void addList( QString text );
    void delList( QString text );
    QString getNickByIndex( int index );    

signals:
    void modeSignal( QString nick, TabContainer::menu_action act );
    void startPrivMsg( QString nick );
    void topicChanged( QString channel, QString topic );

public slots:
    void ShowContextMenu( const QPoint& pos );
    void itemDoubleClicked( QListWidgetItem *item );
    void topicReturnPressed();

private:
    QString name;
    QWidget *tab;
    QVBoxLayout *v_layout;
    QHBoxLayout *h_layout;
    QHBoxLayout *topic_layout;
    QLabel *channel_modes;
    QLineEdit *topic;
    QTextEdit *edit;
    QListWidget *list;

    QVector<user_list> users;
};

#endif // TABCONTAINER_H
