#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    addNewChannelTab( "localhost" );
    warn("Use /server command to connect to server and /help");

    connect( &conn, SIGNAL( parseString(QString) ), this, SLOT( parseString(QString) ) );

    conn.connectHost( "172.16.17.101", "nickname" );
}

MainWindow::~MainWindow()
{
    if( conn.isConnected() )
        conn.disconnect();

    delete ui;
}

void MainWindow::warn( QString text )
{
    if( tabzV.size() > 0 )
        tabzV[0]->append( text );
}

void MainWindow::warnCurrentTab( QString text )
{
    int idx = ui->tabWidget->currentIndex();
    tabzV[idx]->append( text );
}

QString MainWindow::getCurrentTabName()
{
    int index = ui->tabWidget->currentIndex();
    return tabzV[ index ]->getName();
}

void MainWindow::on_lineEdit_returnPressed()
{    
    parseInput( ui->lineEdit->text() );
    ui->lineEdit->clear();    
}

void MainWindow::parseInput( QString text )
{    
    QStringList cmds = text.split( " " );

    if( cmds[0] == "/help" )
    {
        help();
    }
    else if( cmds[0] == "/server" )
    {
        if( cmds.size() >= 2 )
        {
            conn.connectHost( cmds[1], "nickname" );            
        }
    }
    else if( cmds[0] == "/nick" )
    {
        if( cmds.size() >= 2 )
        {
            conn.changeNick( cmds[1] );            
        }
    }
    else if( cmds[0] == "/join" )
    {
        if( cmds.size() >= 2 )
            conn.joinChannel( cmds );
    }
    else if( cmds[0] == "/part" )
    {
        if( cmds.size() >= 2 )
            conn.partChannel( cmds[1] );
    }
    else if( cmds[0] == "/topic" )
    {
        if( cmds.size() >= 2 )
        {
            int index = ui->tabWidget->currentIndex();
            QString chan = tabzV[ index ]->getName();

            setTopic( chan, text.mid(7) );
        }
    }
    else if( cmds[0] == "/kick" )
    {
        if( cmds.size() >= 3 )
            conn.kickNick( cmds[1], cmds[2] );
    }
    else if( cmds[0] == "/msg" )
    {
        if( cmds.size() >= 3 )
        {
            QString nick = cmds[1];
            QString text = catStringList( cmds, 2 );
            QString localmessage = QString("<%1> %2").arg( conn.getNick() ).arg( text );

            int index = startPrivMsg( nick );
            tabzV[ index ]->append( localmessage );
            conn.privMsg( nick, text );
        }
    }
    else if( cmds[0] == "/whois" )
    {
        if( cmds.size() >= 2 )
            conn.whoisNick( cmds[1] );
    }
    else if( cmds[0] == "/mode" )
    {        
        if( cmds.size() == 2 )
            conn.modeRequest( cmds[1] );
        else if( cmds.size() >= 3 )
        {
            cmds.removeFirst();
            conn.modeSet( cmds );
        }
    }
    else if( cmds[0] == "/banlist" )
    {
        if( cmds.size() >= 2 )
            conn.banList( cmds[1] );
    }
    else if( cmds[0] == "/invite" )
    {
        if( cmds.size() >= 3 )
            conn.inviteUser( cmds );
    }
    else if( text.length() > 0 ) // PRIVMSG
    {
        int index = ui->tabWidget->currentIndex();
        QString chan = tabzV[ index ]->getName();

        QString localmessage = QString("<%1> %2").arg( conn.getNick() ).arg( text );
        tabzV[ index ]->append( localmessage );

        conn.privMsg( chan, text );
    }
}

void MainWindow::parseString( QString text )
{                    
//    qDebug() << text;
    text.chop(1);
    QStringList cmd_parts = text.split( " " );

    int numeric = 0;

    if( cmd_parts.size() < 2 )
        return;

    numeric = cmd_parts[1].toInt();

    if( numeric == 0 )
    {
        if( cmd_parts[0].compare("PING") == 0 )
        {
            parsePing( cmd_parts[1] );
        }
        else if( cmd_parts[1].compare("NICK") == 0 )
        {
            parseNick( cmd_parts );
        }
        else if( cmd_parts[1].compare("PART") == 0 )
        {
            parsePart( cmd_parts );
        }
        else if( cmd_parts[1].compare("QUIT") == 0 )
        {
            parseQuit( cmd_parts );
        }
        else if( cmd_parts[1].compare("KICK") == 0 )
        {
            parseKick( cmd_parts );
        }
        else if( cmd_parts[1].compare("MODE") == 0 )
        {            
            parseMode( cmd_parts );
        }
        else if( cmd_parts[1].compare("JOIN") == 0 ) // :nick!~guest@localhost JOIN :#chan
        {
            parseJoin( cmd_parts );
        }
        else if( cmd_parts[1].compare("PRIVMSG") == 0 )
        {
            parsePrivMsg( cmd_parts );
        }
        else if( cmd_parts[1].compare("TOPIC") == 0 )
        {
            parseTopic( cmd_parts );
        }
        else if( cmd_parts[1].compare("INVITE") == 0 )
        {
            warnCurrentTab( QString( "* You have been invited to join %1 by %2").arg( cmd_parts[3] ).arg( cutNick( cmd_parts[0] ) ) );
        }
    }
    else
    {
        switch( numeric )
        {
        case RPL_NAMREPLY:
            parseJoinChannelList( cmd_parts );
            break;

        case RPL_TOPIC:
            parseJoinTopic( cmd_parts );
            break;

        case RPL_CREATEDAT:
        case RPL_TOPIC2:
        case RPL_ENDOFNAMES:
            break;

        case RPL_CHANNELMODEIS:
            parseJoinChannelMode( cmd_parts );
            break;

        case RPL_UMODEIS:        
            warnCurrentTab( catStringList( cmd_parts, 3 ) );
            break;

        case RPL_BANLIST:
            warnCurrentTab( catStringList( cmd_parts, 3 ) );
            break;

        default:            
            if( numeric >= 200 && numeric <=399 )
            {
                warn( text.mid( text.indexOf(":", 1) + 1) );
               // warn( text );
            }
            else if( numeric >= 400 )
            {                
                warnCurrentTab( text.mid( text.indexOf(":", 1) + 1 ) );
            }
        }
    }
}

QString MainWindow::cutNick( QString nick )
{
    return nick.mid( 1, nick.indexOf("!")-1 );
}

int MainWindow::getIndexByChannelName( QString name )
{
    for( int i=0; i<tabzV.size(); i++ )
    {
        if( tabzV[i]->getName().compare( name ) == 0 )
            return i;
    }

    return -1;
}

void MainWindow::parseKick( QStringList part )
{
   QString nick = cutNick( part[0] );
   QString channel = part[2];
   QString victim = part[3];
   QString comment = part[4]; // and higher tbd

   int idx = getIndexByChannelName( channel );

   if( idx == -1 )
       return;

   QString msg = QString("* %1 has kicked %2 from channel %3").arg( nick ).arg( victim ).arg( channel );   

   if( victim == conn.getNick() )
       tabzV[idx]->clearList();
   else
       tabzV[idx]->delList( victim );

   tabzV[idx]->append( msg );
}

void MainWindow::parseNick( QStringList part )
{
    QString old_nick = cutNick( part[0] );
    QString new_nick = cutNick( part[2] );
    QString message = QString("* %1 has changed nick to %2").arg( old_nick ).arg( new_nick );    

    for( auto tab: tabzV )
    {        
        if( tab->changeList( old_nick, new_nick ) == true )
            tab->append( message );
    }

    if( old_nick == conn.getNick() )
        conn.setNick( new_nick );
}

void MainWindow::parsePart( QStringList part )
{
    QString nick = cutNick( part[0] );
    QString channel = part[2];
    QString msg = QString("* %1 has left channel %2").arg( nick ).arg( channel );

    int idx = getIndexByChannelName( channel );

    if( idx != -1 )
    {
        tabzV[idx]->append( msg );

        if( nick == conn.getNick() ) // my part
        {
            tabzV[idx]->clearList();
        }
        else // others part
        {
            tabzV[idx]->delList( nick );
        }
    }
}

void MainWindow::parseQuit( QStringList part )
{
    QString nick = cutNick( part[0] );

    part[2] = part[2].mid( 1 );
    QString msg = QString("* %1 has quit (%2)").arg( nick ).arg( catStringList( part, 2 ) );

    for( int tabs = 1; tabs < ui->tabWidget->count(); tabs++ )
    {
        tabzV[tabs]->delList( nick );
        tabzV[tabs]->append( msg );
    }
}

void MainWindow::parseJoinChannelMode( QStringList mode )
{    
    QString channel = mode[3];    

    int idx = getIndexByChannelName( channel );

    if( idx != -1 )
    {
        QString modes = catStringList( mode, 4 );        
        tabzV[idx]->setMode( modes );
    }
}

void MainWindow::parseJoinTopic( QStringList list )
{
    QString channel = list[3];

    int idx = getIndexByChannelName( channel );

    if( idx != -1 )
    {
        list[4] = list[4].mid( 1 );
        QString topic_value = catStringList( list, 4 );
        tabzV[idx]->setTopic( topic_value );
    }
}

// names and on join
void MainWindow::parseJoinChannelList( QStringList list )
{
    QString channel = list[4];

    int idx = getIndexByChannelName( channel );

    if( idx == -1 )
        return;

    list[5] = list[5].mid( 1 );

    tabzV[idx]->clearList();

    for( int j=5; j<list.size(); j++ )
    {
        tabzV[idx]->addList( list[j] );
    }

    return;
}

void MainWindow::parseMode( QStringList modes )
{
    // MODE user@host #channel +mode params
    QString nick = cutNick( modes[0] );
    QString channel = modes[2];

    int idx = getIndexByChannelName( channel );
    if( idx == -1 )
        return;

    QString info = QString("* %1 has changed mode on %2 to %3").arg( nick ).arg( channel ).arg( catStringList( modes, 3 ) );
    tabzV[idx]->append( info );

    // parse mode[3] here
    //if( modes[3].contains( "+o" ) )

    conn.namesChannel( channel ); // async bad bad slow slow
    conn.modeRequest( channel ); // async mode request bad slow
}

void MainWindow::parseTopic( QStringList topic )
{
    QString nick = cutNick( topic[0] );
    QString channel = topic[2];

    int idx = getIndexByChannelName( channel );

    if( idx != -1 )
    {
        topic[3] = topic[3].mid(1);
        QString topic_value = catStringList( topic, 3 );
        tabzV[idx]->setTopic( topic_value );
        QString info = QString("* %1 has changed topic on %2 to %3").arg( nick ).arg( channel ).arg( topic_value );
        tabzV[idx]->append( info );
    }
}

QString MainWindow::catStringList( QStringList data, int begin )
{
    QString value;

    for( int i=begin; i<data.size(); i++ )
    {
        value.append( data[i] );
        value.append( " " );
    }

    return value;
}

void MainWindow::parsePing( QString ping )
{
    conn.pong( ping );
    warn("PONG");    
}

void MainWindow::parsePrivMsg( QStringList privmsg )
{
    QString channel = privmsg[2];
    QString nick = cutNick( privmsg[0] );

    privmsg[3] = privmsg[3].mid(1);
    QString message = QString("<%1> %2").arg( nick ).arg( catStringList( privmsg, 3 ) );
    QString real_channel;

    if( isChannel( channel ) )
        real_channel = channel;
    else
        real_channel = nick;

    // existing tab
    int idx = getIndexByChannelName( real_channel );

    if( idx != -1 )
    {
            tabzV[idx]->append( message );
            return;
    }

    // new tab
    idx = addNewChannelTab( real_channel );
    tabzV[idx]->append( message );
}

void MainWindow::parseJoin( QStringList join )
{
    QString channel( join[2].mid( 1 ) );
    QString nick = cutNick( join[0] );

    int idx = getIndexByChannelName( channel );

    if( idx != -1 )
    {
        if( nick != conn.getNick() )
            tabzV[idx]->addList( nick );
    }
    else
    {
        idx = addNewChannelTab( channel );
    }

    QString msg = QString("* %1 has joined channel %2").arg( nick ).arg( channel );
    tabzV[idx]->append( msg );
}

int MainWindow::addNewChannelTab( QString name )
{
    TabContainer::tab_type type;

    if( isChannel( name ) )
        type = TabContainer::tab_channel;
    else
        type = TabContainer::tab_private;

    TabContainer *tabc = new TabContainer();    

    QWidget *tab = tabc->addTab( type );
    tabc->setName( name );    

    ui->tabWidget->addTab( tab, name );
    ui->tabWidget->setCurrentIndex( tabzV.size() );

    connect( tabc, SIGNAL( modeSignal(QString, TabContainer::menu_action) ), this, SLOT( modeSlot(QString, TabContainer::menu_action) ) );
    connect( tabc, SIGNAL( startPrivMsg(QString) ), this, SLOT( startPrivMsg(QString) ) );
    connect( tabc, SIGNAL( topicChanged(QString, QString)), this, SLOT( setTopic(QString, QString) ) );

    tabzV.push_back( tabc );

    return tabzV.size()-1;
}

void MainWindow::setTopic( QString channel, QString topic )
{
    conn.setTopic( channel, topic );
}

int MainWindow::startPrivMsg( QString nick )
{
    int index = getIndexByChannelName( nick );

    if( index == -1 )
        index = addNewChannelTab( nick );

    return index;
}

void MainWindow::on_tabWidget_tabCloseRequested( int index )
{
    if( index == 0 )
    {
        QApplication::quit();
    }
    else
    {
        QString channel = tabzV[index]->getName();

        if( isChannel( channel ) )
            conn.partChannel( channel );

        tabzV.remove( index );
        ui->tabWidget->removeTab( index );
    }
}

void MainWindow::modeSlot( QString nick, TabContainer::menu_action act )
{
    QStringList tmp;
    int idx = ui->tabWidget->currentIndex();

    if( idx == 0 )
        return;    

    QString channel = tabzV[idx]->getName();

    switch( act )
    {
    case TabContainer::op: // MODE #channel +o nick
        tmp << channel << "+o" << nick;
        conn.modeSet( tmp );
        break;
    case TabContainer::deop:
        tmp << channel << "-o" << nick;
        conn.modeSet( tmp );
        break;
    case TabContainer::voice:
        tmp << channel << "+v" << nick;
        conn.modeSet( tmp );
        break;
    case TabContainer::devoice:
        tmp << channel << "-v" << nick;
        conn.modeSet( tmp );
        break;
    case TabContainer::kick:
        conn.kickNick( channel, nick );
        break;
    case TabContainer::ban:
        tmp << channel << "+b" << nick;
        conn.modeSet( tmp );
        break;
    }
}

bool MainWindow::isChannel( QString str )
{
    if( str[0] == '#' || str[0] == '&' )
        return true;

    return false;
}

void MainWindow::help()
{
    int index = ui->tabWidget->currentIndex();

    tabzV[ index ]->append( "**************************" );
    tabzV[ index ]->append( "           HELP           " );
    tabzV[ index ]->append( "**************************" );
    tabzV[ index ]->append( "/server hostname" );
    tabzV[ index ]->append( "/nick nickname" );
    tabzV[ index ]->append( "/join #channel" );
    tabzV[ index ]->append( "/part #channel" );
    tabzV[ index ]->append( "/topic topic" );
    tabzV[ index ]->append( "/kick nick" );
    tabzV[ index ]->append( "/msg nick message" );
    tabzV[ index ]->append( "/whois nick" );
    tabzV[ index ]->append( "/mode #channel mode" );
    tabzV[ index ]->append( "/banlist" );
    tabzV[ index ]->append( "/invite nick #channel" );
}
