#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QDebug>

#include "serverconnection.h"
#include "tabcontainer.h"

#define RPL_TOPIC      332
#define RPL_TOPIC2     333
#define RPL_NAMREPLY   353
#define RPL_ENDOFNAMES 366

#define RPL_MOTDSTART 375
#define RPL_MOTD 372
#define RPL_ENDOFMOTD 376

#define RPL_WHOISUSER 311     // "<nick> <user> <host> * :<real name>"
#define RPL_WHOISSERVER 312   // "<nick> <server> :<server info>"
#define RPL_WHOISOPERATOR 313 // "<nick> :is an IRC operator"
#define RPL_WHOISIDLE 317     // "<nick> <integer> :seconds idle"
#define RPL_ENDOFWHOIS 318    // "<nick> :End of WHOIS list"
#define RPL_WHOISCHANNELS 319 // "<nick> :*( ( "@" / "+" ) <channel> " " )"

#define RPL_BANLIST 367       // "<channel> <banmask>"
#define RPL_ENDOFBANLIST 368  //

#define RPL_UMODEIS 221
#define RPL_CHANNELMODEIS 324 // "<channel> <mode> <mode params>"
#define RPL_CREATEDAT 329

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void parseString( QString text );
    void modeSlot( QString nick, TabContainer::menu_action act );    
    void on_lineEdit_returnPressed();    
    void on_tabWidget_tabCloseRequested(int index);
    int startPrivMsg( QString nick );
    void setTopic( QString channel, QString topic );

signals:
    //void dataReceived(QByteArray);

private:
    Ui::MainWindow *ui;
    ServerConnection conn;

    void parseInput( QString text );    

    void warn( QString );
    void warnCurrentTab( QString text );
    int addNewChannelTab( QString name );

    // recv
    void parsePing( QString );
    void parseQuit( QStringList );
    void parsePart( QStringList );
    void parsePrivMsg( QStringList );
    void parseJoin( QStringList );
    void parseTopic( QStringList );
    void parseMode( QStringList );
    void parseNick( QStringList );
    void parseKick( QStringList );
    void parseJoinChannelList( QStringList );
    void parseJoinChannelMode( QStringList );
    void parseJoinTopic( QStringList );

    // useful
    bool isChannel( QString );
    QString addTime(QString);
    QString cutNick( QString nick );
    QString catStringList( QStringList data, int begin );

    int getIndexByChannelName( QString name );
    QString getCurrentTabName();
    void help();

    QVector<TabContainer*> tabzV;
};

#endif // MAINWINDOW_H
