#include "serverconnection.h"

ServerConnection::ServerConnection()
{    
    tcpSocket = new QTcpSocket();
    //connect( tcpSocket, SIGNAL( connected()),this, SLOT(connected()) );
    //connect( tcpSocket, SIGNAL( disconnected()),this, SLOT(disconnected()) );
    connect( tcpSocket, SIGNAL( readyRead()), this, SLOT(readSocket()) );
    connect( tcpSocket, SIGNAL( error(QAbstractSocket::SocketError)), this, SLOT( displayError(QAbstractSocket::SocketError) ) );
}

void ServerConnection::joinChannel( QStringList channel )
{
    writeSocket( "JOIN " );

    for( auto i=1; i<channel.size(); i++ )
    {
        writeSocket( channel[i].toLatin1() );

        if( i != channel.size()-1 )
                writeSocket( " " );
    }

    writeSocket( "\n" );

    modeRequest( channel[1] );
}

void ServerConnection::partChannel( QString channel )
{
    writeSocket( "PART " );
    writeSocket( channel.toLatin1() );
    writeSocket( "\n" );
}

void ServerConnection::namesChannel( QString channel )
{
    writeSocket( "NAMES " );
    writeSocket( channel.toLatin1() );
    writeSocket( "\n" );
}

void ServerConnection::changeNick( QString nick )
{
    writeSocket( "NICK " );
    writeSocket( nick.toLatin1() );
    writeSocket( "\n" );    
}

void ServerConnection::kickNick( QString channel, QString nick )
{
    writeSocket( "KICK " );
    writeSocket( channel.toLatin1() );
    writeSocket( " " );
    writeSocket( nick.toLatin1() );
    writeSocket( "\n" );
}

void ServerConnection::privMsg( QString nick, QString text )
{
    QString privmsg = QString("PRIVMSG ") + nick + " :" + text + "\n";
    writeSocket( privmsg.toUtf8() );
}

void ServerConnection::whoisNick( QString nick )
{
    writeSocket( "WHOIS " );
    writeSocket( nick.toLatin1() );
    writeSocket( "\n" );
}

void ServerConnection::modeRequest( QString chan )
{
    writeSocket( "MODE " );
    writeSocket( chan.toLatin1() );
    writeSocket( "\n" );
}

void ServerConnection::modeSet( QStringList mode )
{    
    writeSocket( "MODE" );

    for( int i=0; i<mode.size(); i++ )
    {
        writeSocket( " " );
        writeSocket( mode[i].toLatin1() );
    }
    writeSocket( " \n" );
}

void ServerConnection::banList( QString chan )
{
    writeSocket( "MODE " );
    writeSocket( chan.toLatin1() );
    writeSocket( " +b\n" );
}

void ServerConnection::inviteUser( QStringList inv )
{
    writeSocket( "INVITE " );
    writeSocket( inv[1].toLatin1() );
    writeSocket( " " );
    writeSocket( inv[2].toLatin1() );
    writeSocket( "\n" );
}

void ServerConnection::setTopic( QString chan, QString topic )
{
    QString privmsg = QString("TOPIC ") + chan + " :" + topic + "\n";
    writeSocket( privmsg.toUtf8() );
}

void ServerConnection::pong( QString host )
{
    writeSocket("PONG ");
    writeSocket( host.toLatin1() );
}

bool ServerConnection::connectHost( QString server, QString nick )
{
    tcpSocket->abort();
    tcpSocket->connectToHost( server, 6667 );

    if( tcpSocket->waitForConnected( 3000 ) == false )
    {
        qDebug() << "fial";
        return false;
    }

    if( !writeSocket("USER guest 0 * :Ronnie Reagan\n") )
        return false;

    changeNick( nick );
    setNick( nick );

    return true;
}

bool ServerConnection::isConnected()
{
    if( tcpSocket->state() == QAbstractSocket::ConnectedState )
        return true;

    return false;
}

void ServerConnection::disconnect()
{
    if( isConnected() )
    {
        // send QUIT
        tcpSocket->disconnectFromHost();
    }
}

void ServerConnection::connected()
{
    qDebug() << "Connected";
    //warn( "Server connected" );
}

void ServerConnection::disconnected()
{
    qDebug() << "Disconnected";
    //warn( "Disconnected" );
}

void ServerConnection::readSocket()
{
    QString data = tcpSocket->readAll();
    QStringList data_parts = data.split( "\n" );

    for( auto i: data_parts )
    {
        emit parseString( i );
    }
}

bool ServerConnection::writeSocket( QByteArray data )
{
    if( tcpSocket->state() != QAbstractSocket::ConnectedState )
        return false;

    tcpSocket->write( data );

    return tcpSocket->waitForBytesWritten( 1000 );
}

void ServerConnection::displayError( QAbstractSocket::SocketError socketError )
{
    switch( socketError )
    {
    case QAbstractSocket::RemoteHostClosedError:
        break;

    case QAbstractSocket::HostNotFoundError:
        //warnCurrentTab("The host was not found. Please check the host name and port settings.");
        break;

    case QAbstractSocket::ConnectionRefusedError:
        //warnCurrentTab("The connection was refused by the peer.");
        break;

    default:
        QString message = QString("The following error occurred: %1.").arg( tcpSocket->errorString() );
        //warnCurrentTab( message );
    }
}
