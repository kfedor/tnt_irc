#ifndef SERVERCONNECTION_H
#define SERVERCONNECTION_H

#include <QObject>
#include <QTcpSocket>
#include <QDebug>

class ServerConnection : public QObject
{
    Q_OBJECT

public:
    ServerConnection();

    bool connectHost( QString, QString );
    void disconnect();
    bool isConnected();

    void whoisNick( QString );
    void privMsg( QString, QString );
    void joinChannel( QStringList );
    void partChannel( QString );
    void namesChannel( QString );
    void changeNick( QString );
    void kickNick( QString, QString );
    void setTopic( QString, QString );
    void inviteUser( QStringList inv );
    void modeRequest( QString );
    void modeSet( QStringList mode );
    void banList( QString );
    void pong( QString host );

    QString getNick() { return nickname; }
    void setNick( QString nick ) { nickname = nick; }

signals:
    void parseString( QString );

private slots:
    void readSocket();
    void displayError( QAbstractSocket::SocketError socketError );
    void connected();
    void disconnected();

private:
    QString nickname;
    QTcpSocket *tcpSocket;
    bool writeSocket(QByteArray data);
};

#endif // SERVERCONNECTION_H
