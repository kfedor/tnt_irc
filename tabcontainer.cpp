#include "tabcontainer.h"

QString TabContainer::getNickByIndex( int index )
{
    QListWidgetItem *item = list->item( index );
    QString list_nick = item->text();

    list_nick = list_nick.mid( list_nick.indexOf("]", 1) + 1 );

    return list_nick;
}

void TabContainer::clearList()
{
    users.clear();
    list->clear();
}

void TabContainer::redrawList()
{
    list->clear();
    //  qSort( users );

    for( auto user: users )
    {
        QString nick;

        if( user.modes.length() > 0 )
            nick = "[" + user.modes + "]" + user.nick;
        else
            nick = user.nick;

        list->addItem( nick );
    }
}

bool TabContainer::changeList( QString oldnick, QString newnick )
{
    for( auto &user: users )
    {
        if( user.nick == oldnick )
        {
            user.nick = newnick;
            redrawList();
            return true;
        }
    }

    return false;
}

// parse nick with modes
void TabContainer::addList( QString text )
{
    //const QRegularExpression letters("//<([a-zA-Z\\[\\]\\\\`_\\^\\{\\|\\}][a-zA-Z0-9\\[\\]\\\\`_\\^\\{\\|\\}-]{1,31})>//");
    const QRegularExpression letters("[a-zA-Z0-9_-\\{}^`|\\[\\]]+");

    if( list == NULL )
        return;

    int q = text.indexOf( letters, 0 );

    user_list item;
    item.modes = text.mid( 0, q );
    item.nick = text.mid( q, text.length() );

    users.push_back( item );

    redrawList();
}

void TabContainer::delList( QString text )
{
    if( list == NULL )
        return;

    // delete from users
    for( auto i=0; i<users.size(); i++ )
    {
        if( users[i].nick == text )
        {
            users.erase( users.begin()+i );
            break;
        }
    }

    redrawList();
}



QWidget *TabContainer::addTab( TabContainer::tab_type ttype )
{
    tab = new QWidget();
    v_layout = new QVBoxLayout( tab );
    h_layout = new QHBoxLayout();
    topic_layout = new QHBoxLayout();

    edit = new QTextEdit( tab );                   // main edit field
    edit->setReadOnly(true);
    edit->setFocusPolicy( Qt::NoFocus );

    //QPalette p = edit->palette();
    //p.setColor(QPalette::Base, QColor(240, 240, 255));
    //edit->setPalette(p);

    //edit->setStyleSheet("background: black; color: lightgrey");
    //edit->setStyleSheet("background-image: url(bg.png); background-attachment: fixed;");

    topic = NULL;
    channel_modes = NULL;
    list = NULL;

    h_layout->addWidget( edit );

    if( ttype == TabContainer::tab_channel )
    {
        topic = new QLineEdit( tab );              // topic
        channel_modes = new QLabel( tab );         // modes
        list = new QListWidget( tab );             // list users

        //list->setStyleSheet("background: black; color: lightgrey");
        //topic->setStyleSheet("background: black; color: lightgrey");

        topic_layout->addWidget( topic );
        topic_layout->addWidget( channel_modes );

        topic_layout->setStretch( 0, 80 );
        topic_layout->setStretch( 1, 20 );

        v_layout->addLayout( topic_layout );
        h_layout->addWidget( list );

        list->setContextMenuPolicy( Qt::CustomContextMenu );
        connect( list, SIGNAL( customContextMenuRequested( const QPoint & ) ), this, SLOT( ShowContextMenu( const QPoint & ) ) );                
        connect( list, SIGNAL( itemDoubleClicked(QListWidgetItem*)), this, SLOT( itemDoubleClicked(QListWidgetItem*) ) );
        connect( topic, SIGNAL( returnPressed() ), this, SLOT( topicReturnPressed() ) );
    }

    v_layout->addLayout( h_layout );
    h_layout->setStretch( 0, 80 );
    h_layout->setStretch( 1, 20 );    

    return tab;
}

void TabContainer::topicReturnPressed()
{
    emit topicChanged( name, topic->text() );
}

void TabContainer::itemDoubleClicked( QListWidgetItem *item )
{
    QString list_nick = item->text().mid( item->text().indexOf("]", 1) + 1 );
    emit startPrivMsg( list_nick );
}

void TabContainer::ShowContextMenu( const QPoint &pos )
{
    QModelIndex index = list->indexAt( pos );
    QPoint globalPos = list->mapToGlobal( pos );

    QMenu myMenu;
    myMenu.addAction("Give op")->setData(1);
    myMenu.addAction("Give voice")->setData(2);
    myMenu.addAction("Take op")->setData(3);
    myMenu.addAction("Take voice")->setData(4);
    myMenu.addSeparator();
    myMenu.addAction("Kick")->setData(5);
    myMenu.addAction("Ban")->setData(6);

    QAction* selectedItem = myMenu.exec( globalPos );

    if( selectedItem )
    {        
        emit modeSignal( getNickByIndex( index.row() ), (menu_action) selectedItem->data().toInt()  );
    }
}

QString TabContainer::addTime( QString data )
{
    QTime time = QTime::currentTime();
    QString out = "[" + time.toString() + "] " + data;
    return out;
}
